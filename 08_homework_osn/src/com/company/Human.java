package com.company;

public class Human {
    private Double weit;
    private String name;
    private   int stepsCount;



    public Double getWeit() {
        return  this.weit;
    }



    public void setWeit(double weit) {
        if (weit < 0 || weit > 300) {
            weit = 0;
        }
        this.weit = weit;
    }

    public String getName () {
        return name;
    }


    public void setName (String name){
        this.name = name;
    }
}